/*
Package text contains functions to apply various formatting elements to text when printing to a terminal that supports the features.
Formatting elements include colors (like Blue, Green, etc.) and styles (like Bold, Italics, etc.), which are exposed through the [Colors] and [Styles] structs.

To see examples of how to use the functions and features in this package, check out the examples_test.go file.
You can check which features your terminal supports by running the following command from the root of the package:

	go test ./terminal_test.go -v

# Basic formatting (Apply functions)

The most basic approach to formatting text using this package involves using the functions exposed by the [Apply] struct:

	goText := textfmt.Apply.Highlight("Go", textfmt.Colors.Yellow)
	fmt.Println("My favorite programming language is " + goText + "!") // The word "Go" is highlighted yellow

# Templating (Formats)

The package also enables templating features through the use of user-defined Formats.
Each [Format] contains a text color (which can be set to the terminal default), a background/highlight color (which can be set to the terminal default), and zero or more styles.

Formats allow specific combinations of formatting elements to be repeatedly applied without the need to declare those elements each time.
For example, one could create a header Format specifying green, bold, underlined text:

	header := textfmt.MakeFormat(textfmt.Colors.Green, textfmt.Colors.Default, textfmt.Styles.Bold, textfmt.Styles.Underline)

Once the Format is created, printing a new header is as simple as using the Format's built-in Print, Printf, or Println method:

	header.Print("I'm a formatted header!") // Prints green, bold, underlined text

You can also use Formats to generate strings directly using the Sprint equivalent of any of the methods mentioned above.

	headerString := header.Sprint("Hello Gophers")
	fmt.Print(headerString + " and others!") // Prints green, bold underlined text for first 2 words only

Formats have optimizations to make applying them faster than using functions in the Apply struct to achieve the same result, but creating them has an overhead.
Use functions in the Apply struct for formatting element combinations that are only used once or twice while reserving Formats for combinations that are expected to be reused.

# Inlines (Inline codes)

You can also use inline formatting codes (exposed by the [Inlines] struct) to directly control the formatting of textfmt.
Due to the complexity and potential for human error involved, this method is not recommended.

Inline codes are disabled by default and can be enabled by running the Inlines.Initialize() function.

# Customization

There are a few options to customize the output of this library (with more planned).

Color will be disabled if any of the following are true:
  - The NO_COLOR environment variable is set to a non-empty string
  - The TERM environment variable is set to "dumb" (unless overridden by the FORCE_COLOR environment variable)
  - The exported NoColor variable is set to true

# Behavior quirks

Certain uses, especially involving inline formatting codes or attempting to "stack" formatting elements, can result in unexpected behaviors.
Some of these are described below.

Format methods and Apply functions that set a text color or background/highlight color will revert to the terminal's default color at the end of the textfmt.

Active styles will remain active through text that does not include them. Styles will be disabled at the end of any text that does include them, even if they were active before the text started.

Customizations (described in the section above) are checked at different times depending on the approach used:
  - Apply functions check for customizations at the time they are run
  - Formats check for customizations whenever a method of the format is run
  - Inline codes check for customizations each time Inlines.Initialize() is called

This library may not always detect output locations that don't supports ANSI codes. Developers should make use of the customization features described above to avoid unexpected characters when printing or piping output to a file.
*/
package textfmt
