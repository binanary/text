# Text Formatting

A library for applying colors and styles to text in the terminal, desigend for powerful templating and optimized for modern text editor features like IntelliSense.

Supports bold, underline, strikethrough and more styles, in addition to text and background colors.

Compatible with VT100 (ANSI) terminals, which covers most popular and out-of-the-box terminals. If you want even more control over a terminal, this library can be used alongside [Tiny Terminal](https://gitlab.com/binanary/terminal).

## Installation

![Latest Version](https://gitlab.com/binanary/textfmt/-/badges/release.svg?key_text=Latest%20Version)

To add the library to your codebase, use the command `go get gitlab.com/binanary/textfmt`

## Usage
There are 3 different ways apply formatting elements to text using this library.

- **Apply functions** are ideal for formatting element combinations that will only be used once or twice
- **Formats** provide templating functionality and are best for formatting element combinations that will be used multiple times or changed at runtime
- **Inline codes** give you maximum control over formatting with minimal processing overhead, but also lack protections against human error, potentially resulting in more bugs and unexpected behaviors

Extensive documentation for each approach can be found on the [Go package website](https://pkg.go.dev/gitlab.com/binanary/text#section-documentation)

In addition, the [examples_test.go](./examples_test.go) file contains functioning implementations of each approach.

### Accessibility

This package supports the [NO_COLOR environment variable](https://no-color.org/).

## Support
If you encounter a bug or need help using this library, please [create an issue](https://gitlab.com/binanary/text/-/issues/new).

## Roadmap
See [open issues](https://gitlab.com/binanary/text/-/issues) for in-development features and roadmap items.

## Contributing
Anybody is welcome to submit a merge request with changes that they believe fit the spirit of the project. Feature requests and suggestions can be submitted using GitLab issues.

All suggested changes will be discussed, but the project's maintainers have the final say in approvals and rejections.

All approved contributions are subject to the project's [license](./LICENSE). Do not contribute if you don't want your changes to be open source.

## License

This library is open-sourced under the [Boost Software License](./LICENSE). This license does not apply to code from open or rejected merge requests.