package textfmt_test

import (
	"fmt"
	"testing"

	"gitlab.com/binanary/textfmt"
)

// These tests produce human-viewable output and will never automatically fail (unless an error occurs). They should be run with the -v flag.

// TestDoc runs the examples provided in doc.go with some minor alterations to ensure the output is formatted nicely. It demonstrates the use of Apply functions and formats.
func TestDoc(t *testing.T) {
	// Apply functions
	goText := textfmt.Apply.Highlight("Go", textfmt.Colors.Yellow)
	fmt.Println("My favorite programming language is " + goText + "!") // The word "Go" is highlighted yellow

	// Formats
	header := textfmt.MakeFormat(textfmt.Colors.Green, textfmt.Colors.Default, textfmt.Styles.Bold, textfmt.Styles.Underline)
	header.Println("I'm a formatted header!") // Prints green, bold, underlined text

	headerString := header.Sprint("Hello Gophers")
	fmt.Print(headerString + " and others!") // Prints green, bold underlined text for first 2 words only

	fmt.Print("\n")
}

// To reduce excessive dot notation for colors and styles you use a lot, you can assign them to your own variables.
func TestCustomNames(t *testing.T) {
	red := textfmt.Colors.Red
	fmt.Println(textfmt.Apply.Color("This text is red!", red))
}

// Inline codes allow you to change the formatting directly.
// Note that the separate print statements in this function are for readability only.
// Ending one print statement and starting another does not have any effect on formatting, and formatting codes can be used at any point within a print statement.
func TestInlines(t *testing.T) {
	textfmt.Inlines.Initialize()
	fmt.Print(textfmt.Inlines.StartGreenText + "This text will be green " + textfmt.Inlines.StartUnderline + "and now underlined too.")
	fmt.Println(textfmt.Inlines.RevertTextColor + " But now it's just underlined." + textfmt.Inlines.EndUnderline)

	fmt.Print(textfmt.Inlines.StartRedBackground + "You can change the background color")
	fmt.Println(textfmt.Inlines.ResetFormat + " and reset everything too.")
}

// You can disable the initialization function to prevent inline codes from ever working in your codebase.
// Do this in an init function for best results.
// This test may produce unexpected output when run alongside tests that run Inlines.Initialize.
func TestDisableInlines(t *testing.T) {
	textfmt.Inlines.Initialize = func() {}
	textfmt.Inlines.Initialize()
	fmt.Println(textfmt.Inlines.StartGreenText + "This text won't be green" + textfmt.Inlines.RevertTextColor)
}
