package textfmt_test

import (
	"fmt"
	"testing"

	"gitlab.com/binanary/textfmt"
)

// Tests in this file require visual inspection to determine whether or not they passed

// TestTerminal attempts to apply all text colors, background colors, and styles one at a time.
//
// Command (run from root of project):
//
//	go test -run ^TestTerminal$ -v
func TestTerminal(t *testing.T) {
	fmt.Println("============================= TERMINAL TEST =============================")
	blacktc := textfmt.Apply.Color("Black", textfmt.Colors.Black)
	redtc := textfmt.Apply.Color("Red", textfmt.Colors.Red)
	greentc := textfmt.Apply.Color("Green", textfmt.Colors.Green)
	yellowtc := textfmt.Apply.Color("Yellow", textfmt.Colors.Yellow)
	bluetc := textfmt.Apply.Color("Blue", textfmt.Colors.Blue)
	magentatc := textfmt.Apply.Color("Magenta", textfmt.Colors.Magenta)
	cyantc := textfmt.Apply.Color("Cyan", textfmt.Colors.Cyan)
	whitetc := textfmt.Apply.Color("White", textfmt.Colors.White)
	deftc := textfmt.Apply.Color("Default", textfmt.Colors.Default)
	fmt.Println("Text Colors:      ", blacktc, redtc, greentc, yellowtc, bluetc, magentatc, cyantc, whitetc, deftc)

	blackbg := textfmt.Apply.Highlight("Black", textfmt.Colors.Black)
	redbg := textfmt.Apply.Highlight("Red", textfmt.Colors.Red)
	greenbg := textfmt.Apply.Highlight("Green", textfmt.Colors.Green)
	yellowbg := textfmt.Apply.Highlight("Yellow", textfmt.Colors.Yellow)
	bluebg := textfmt.Apply.Highlight("Blue", textfmt.Colors.Blue)
	magentabg := textfmt.Apply.Highlight("Magenta", textfmt.Colors.Magenta)
	cyanbg := textfmt.Apply.Highlight("Cyan", textfmt.Colors.Cyan)
	whitebg := textfmt.Apply.Highlight("White", textfmt.Colors.White)
	defbg := textfmt.Apply.Highlight("Default", textfmt.Colors.Default)
	fmt.Println("Background Colors:", blackbg, redbg, greenbg, yellowbg, bluebg, magentabg, cyanbg, whitebg, defbg)

	bolds := textfmt.Apply.Styles("Bold", textfmt.Styles.Bold)
	italicss := textfmt.Apply.Styles("Italics", textfmt.Styles.Italics)
	underlines := textfmt.Apply.Styles("Underline", textfmt.Styles.Underline)
	strikethroughs := textfmt.Apply.Styles("Strikethrough", textfmt.Styles.Strikethrough)
	dims := textfmt.Apply.Styles("Dim", textfmt.Styles.Dim)
	blinkings := textfmt.Apply.Styles("Blinking", textfmt.Styles.Blinking)
	inverts := textfmt.Apply.Styles("Invert", textfmt.Styles.Invert)
	fmt.Println("Styles:         ", bolds, italicss, underlines, strikethroughs, dims, blinkings, inverts)
	fmt.Println("=========================== END TERMINAL TEST ===========================")
}

// TestFormat applies each method of Format on the same Format and allows the user to compare the results.
// The output should be a string printed 6 times on separate lines with the same formatting each time.
func TestFormat(t *testing.T) {
	f.Print(s)
	fmt.Println()
	f.Println(s)
	f.Printf(s)
	fmt.Println()

	s1 := f.Sprint(s)
	s2 := f.Sprintln(s)
	s3 := f.Sprintf(s)
	fmt.Println(s1)
	fmt.Print(s2)
	fmt.Print(s3)
	fmt.Println()
}
