package textfmt

// go test -run whitebox$

// The tests in this file will generate a string and check the output to see if it contains the correct start and end codes.

import (
	"math/rand"
	"os"
	"regexp"
	"slices"
	"strings"
	"testing"
)

type testFormats interface {
	Color | Style
}

var testColors = []Color{Colors.Red, Colors.Green, Colors.Black, Colors.Yellow, Colors.Blue, Colors.Magenta, Colors.Cyan, Colors.White, Colors.Default}
var testStyles = []Style{Styles.Bold, Styles.Italics, Styles.Underline, Styles.Strikethrough, Styles.Dim, Styles.Blinking, Styles.Invert}

var testRegex, _ = regexp.Compile(`^\x1B\[([0-9;]+)+mThis is a test string\x1B\[([0-9;]+)+m`)

func random[F testFormats](arr []F) F {
	return arr[rand.Intn(len(arr)-1)]
}

func randomColor() Color {
	return random(testColors)
}

func randomStyle() Style {
	return random(testStyles)
}

func compareCodes(sc, ec, correctSc, correctEc []string) (ok bool) {
	slices.Sort(sc)
	slices.Sort(ec)
	slices.Sort(correctSc)
	slices.Sort(correctEc)
	if slices.Equal(sc, correctSc) && slices.Equal(ec, correctEc) && len(sc) == 4 && len(ec) == 4 {
		return true
	}
	return false
}

func TestApplyAll(t *testing.T) {
	os.Unsetenv("NO_COLOR")
	tc, bc, s1, s2 := randomColor(), randomColor(), randomStyle(), randomStyle()
	for s2 == s1 {
		s2 = random(testStyles)
	}
	s := Apply.ColorsAndStyles("This is a test string", tc, bc, s1, s2)
	matches := testRegex.FindAllSubmatch([]byte(s), -1)[0]
	sc := strings.Split(string(matches[1]), ";")
	ec := strings.Split(string(matches[2]), ";")
	correctSc := []string{tc.text, bc.bg, s1.start, s2.start}
	correctEc := []string{Colors.Default.text, Colors.Default.bg, s1.end, s2.end}
	if !compareCodes(sc, ec, correctSc, correctEc) {
		t.FailNow()
	}
}

func TestFormatSprint(t *testing.T) {
	os.Unsetenv("NO_COLOR")
	tc, bc, s1, s2 := randomColor(), randomColor(), randomStyle(), randomStyle()
	for s2 == s1 {
		s2 = random(testStyles)
	}
	f := MakeFormat(tc, bc, s1, s2)
	s := f.Sprint("This is a test string")
	matches := testRegex.FindAllSubmatch([]byte(s), -1)[0]
	sc := strings.Split(string(matches[1]), ";")
	ec := strings.Split(string(matches[2]), ";")
	correctSc := []string{tc.text, bc.bg, s1.start, s2.start}
	correctEc := []string{Colors.Default.text, Colors.Default.bg, s1.end, s2.end}
	if !compareCodes(sc, ec, correctSc, correctEc) {
		t.FailNow()
	}
}
