package textfmt

import (
	"fmt"
	"strings"
)

// Exported only to enable returning formats from functions.
type Format struct {
	start   string
	startNC string
	end     string
	endNC   string
	// textColor       color
	// backgroundColor color
	// styles          []Style
}

// MakeFormat returns a reusable custom format that specifies a textColor, highlightColor, and one or more styles.
// Use Colors.Default for any color parameters that you don't want to set.
func MakeFormat(textColor Color, highlightColor Color, styles ...Style) Format {
	startCode, endCode := buildFullCodes(textColor, highlightColor, styles)
	var startCodeNC, endCodeNC string
	if len(styles) > 0 {
		var startCodeNCB, endCodeNCB strings.Builder
		buildStyleCodes(styles, &startCodeNCB, &endCodeNCB)
		startCodeNC, endCodeNC = buildCode(startCodeNCB.String()), buildCode(endCodeNCB.String())
	} else {
		startCodeNC, endCodeNC = "", ""
	}

	return Format{
		start:   startCode,
		end:     endCode,
		startNC: startCodeNC,
		endNC:   endCodeNC,
		// textColor:       textColor,
		// backgroundColor: backgroundColor,
		// styles:          styles,
	}
}

/* func NewFormat() format {
	defaultCode := buildCode(Colors.Default.text + ";" + Colors.Default.bg)
	return format{
		start:           defaultCode,
		end:             defaultCode,
		textColor:       Colors.Default,
		backgroundColor: Colors.Default,
		styles:          []Style{},
	}
} */

// A wrapper around fmt.Print that applies a specified format
func (f Format) Print(text ...any) {
	if noColor() {
		fmt.Print(f.Sprint(text...))
	}
	fmt.Print(f.Sprint(text...))
}

// A wrapper around fmt.Printf that applies a specified format
func (f Format) Printf(format string, a ...any) {
	if noColor() {
		fmt.Print(f.Sprintf(format, a...))
	}
	fmt.Print(f.Sprintf(format, a...))
}

// A wrapper around fmt.Println that applies a specified format
func (f Format) Println(text ...any) {
	if noColor() {
		fmt.Println(f.Sprint(text...))
	}
	fmt.Println(f.Sprint(text...))
}

// A wrapper around fmt.Sprint that applies a specified format
func (f Format) Sprint(a ...any) string {
	if noColor() {
		return f.startNC + fmt.Sprint(a...) + f.endNC
	}
	return f.start + fmt.Sprint(a...) + f.end
}

// A wrapper around fmt.Sprintf that applies a specified format
func (f Format) Sprintf(format string, a ...any) string {
	if noColor() {
		return f.startNC + fmt.Sprintf(format, a...) + f.endNC
	}
	return f.start + fmt.Sprintf(format, a...) + f.end
}

// A wrapper around fmt.Sprintln that applies a specified format
func (f Format) Sprintln(text ...any) string {
	if noColor() {
		return f.startNC + fmt.Sprint(text...) + f.endNC + "\n"
	}
	return f.start + fmt.Sprint(text...) + f.end + "\n"
}

type apply struct {
	// Apply.ColorsAndStyles returns a new string that will have colored text, a colored background (highlight), and a styled appearance when printed to a terminal that supports the features.
	ColorsAndStyles func(text string, textColor Color, highlightColor Color, styles ...Style) string

	// Apply.Styles returns a new string that will have a styled appearance when printed to a terminal that supports the features.
	Styles func(text string, styles ...Style) string

	// Apply.Color returns a new string that will have colored text when printed to a terminal that supports the features.
	Color func(text string, color Color) string

	// Apply.Highlight returns a new string that will have a colored background when printed to a terminal that supports the feature.
	Highlight func(text string, highlightColor Color) string
}

// Apply contains functions that can be used to quickly format text.
// All of its functions return a new string that can be passed into the printing functions in Go's fmt package.
var Apply = apply{
	ColorsAndStyles: applyAll,
	Styles:          applyStyles,
	Color:           applyColor,
	Highlight:       applyBackgroundColor,
}

func applyAll(s string, tc Color, bc Color, fs ...Style) string {
	if noColor() {
		return applyStyles(s, fs...)
	}

	startCode, endCode := buildFullCodes(tc, bc, fs)
	return startCode + s + endCode
}

func applyStyles(s string, fs ...Style) string {
	if len(fs) == 0 {
		return s
	}

	var startCode, endCode strings.Builder
	buildStyleCodes(fs, &startCode, &endCode)
	return buildCode(startCode.String()) + s + buildCode(endCode.String())
}

func applyColor(s string, c Color) string {
	if noColor() {
		return s
	}

	return buildCode(c.text) + s + buildCode(Colors.Default.text)
}

func applyBackgroundColor(s string, c Color) string {
	if noColor() {
		return s
	}

	return buildCode(c.bg) + s + buildCode(Colors.Default.bg)
}
