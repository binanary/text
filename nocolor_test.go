package textfmt_test

import (
	"fmt"
	"os"
	"testing"

	"gitlab.com/binanary/textfmt"
)

var (
	afs            = "Apply.Format"
	acs            = "Apply.Color"
	ahs            = "Apply.Highlight"
	aas            = "Apply.ColorsAndStyles"
	af, ac, ah, aa string
)

func assertEq(s1, s2 string) bool {
	return s1 == s2
}

func testNoColorAux(fns textfmt.Format) {
	af = fns.Sprint(afs)
	ac = textfmt.Apply.Color(acs, tc)
	ah = textfmt.Apply.Highlight(ahs, bc)
	aa = textfmt.Apply.ColorsAndStyles(aas, tc, bc)
	fmt.Println(af, ac, ah, aa)
}

// TestNoColor will automatically fail if any Apply functions modify strings when the NO_COLOR environment variable is set to a non-string value and no styles are passed into the functions.
// Run with -v to visually check that the functions are formatting text correctly
func TestNoColor(t *testing.T) {

	fns := textfmt.MakeFormat(tc, bc)

	fmt.Println(textfmt.Apply.Styles("NO_COLOR set", textfmt.Styles.Bold, textfmt.Styles.Underline))
	os.Setenv("NO_COLOR", "1")
	testNoColorAux(fns)

	if !assertEq(af, afs) || !assertEq(ac, acs) || !assertEq(ah, ahs) || !assertEq(aa, aas) {
		t.FailNow()
	}

	os.Unsetenv("NO_COLOR")
	fmt.Println(textfmt.Apply.Styles("NO_COLOR unset", textfmt.Styles.Bold, textfmt.Styles.Underline))
	testNoColorAux(fns)
}
