package textfmt

import "os"

// Exported only to enable returning colors from functions.
type Color struct {
	text string
	bg   string
}

var NoColor = false

var (
	black = Color{ // 0
		text: "30",
		bg:   "40",
	}
	red = Color{ // 1
		text: "31",
		bg:   "41",
	}
	green = Color{ // 2
		text: "32",
		bg:   "42",
	}
	yellow = Color{ // 3
		text: "33",
		bg:   "43",
	}
	blue = Color{ // 4
		text: "34",
		bg:   "44",
	}
	magenta = Color{ // 5
		text: "35",
		bg:   "45",
	}
	cyan = Color{ // 6
		text: "36",
		bg:   "46",
	}
	white = Color{ // 7
		text: "37",
		bg:   "47",
	}
	def = Color{ // 9
		text: "39",
		bg:   "49",
	}
	nocolor = Color{ // 9
		text: "",
		bg:   "",
	}
)

type colors struct {
	Red     Color
	Green   Color
	Black   Color
	Yellow  Color
	Blue    Color
	Magenta Color
	Cyan    Color
	White   Color
	Default Color
	None    Color
}

// Colors that can be passed into various functions.
// Included colors can be used as both text and background/highlight colors (depending on terminal support).
var Colors = colors{
	Black:   black,
	Red:     red,
	Green:   green,
	Yellow:  yellow,
	Blue:    blue,
	Magenta: magenta,
	Cyan:    cyan,
	White:   white,
	Default: def,
	None:    nocolor,
}

// noColor checks for situations where color should be disabled.
// Returns true if color should not be printed, false otherwise.
func noColor() bool {
	if os.Getenv("NO_COLOR") != "" || NoColor {
		return true
	}
	if os.Getenv("TERM") == "dumb" {
		if os.Getenv("FORCE_COLOR") == "" {
			return true
		}
	}
	return false
}
