package textfmt

import "strings"

var prefix = "\x1B["

func buildCode(s string) string {
	return prefix + s + "m"
}

func buildFullCodes(tc Color, bc Color, fs []Style) (sc string, ec string) {
	var startCode, endCode strings.Builder
	if len(fs) > 0 {
		buildStyleCodes(fs, &startCode, &endCode)
		startCode.WriteString(";")
		endCode.WriteString(";")
	}

	startCode.WriteString(tc.text)
	endCode.WriteString(Colors.Default.text)

	startCode.WriteString(";")
	endCode.WriteString(";")

	startCode.WriteString(bc.bg)
	endCode.WriteString(Colors.Default.bg)

	sc = buildCode(startCode.String())
	ec = buildCode(endCode.String())
	return sc, ec
}

// Does not add prefix or suffix
func buildStyleCodes(fs []Style, sc *strings.Builder, ec *strings.Builder) {
	for i, f := range fs {
		if i != 0 {
			sc.WriteString(";")
			ec.WriteString(";")
		}
		sc.WriteString(f.start)
		ec.WriteString(f.end)
	}
}
