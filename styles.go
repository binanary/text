package textfmt

// Exported only to enable returning styles from functions and creating lists of styles.
type Style struct {
	start string
	end   string
}

var (
	bold = Style{ // 1
		start: "1",
		end:   "22", // Intentional
	}
	dim = Style{ // 2
		start: "2",
		end:   "22",
	}
	italics = Style{ // 3
		start: "3",
		end:   "23",
	}
	underline = Style{ // 4
		start: "4",
		end:   "24",
	}
	blinking = Style{ // 5
		start: "5",
		end:   "25",
	}
	invert = Style{ // 7
		start: "7",
		end:   "27",
	}
	strikethrough = Style{ // 9
		start: "9",
		end:   "29",
	}
)

type styles struct {
	Bold          Style
	Italics       Style
	Underline     Style
	Strikethrough Style
	Dim           Style
	Blinking      Style
	Invert        Style
}

// Styles that can be passed into various functions.
// Full functionality is dependent on terminal support.
var Styles = styles{
	Bold:          bold,
	Italics:       italics,
	Underline:     underline,
	Strikethrough: strikethrough,
	Dim:           dim,
	Blinking:      blinking,
	Invert:        invert,
}
