package textfmt

var inlinesInitialized bool = false

type inlines struct {
	Initialize func()

	// Styles

	StartBold string
	EndBold   string

	StartItalics string
	EndItalics   string

	StartUnderline string
	EndUnderline   string

	StartStrikethrough string
	EndStrikethrough   string

	StartDim string
	EndDim   string

	StartBlinking string
	EndBlinking   string

	StartInvert string
	EndInvert   string

	ResetFormat string

	// Text colors

	StartBlackText   string
	StartRedText     string
	StartGreenText   string
	StartYellowText  string
	StartBlueText    string
	StartMagentaText string
	StartCyanText    string
	StartWhiteText   string
	RevertTextColor  string

	// Background colors

	StartBlackBackground   string
	StartRedBackground     string
	StartGreenBackground   string
	StartYellowBackground  string
	StartBlueBackground    string
	StartMagentaBackground string
	StartCyanBackground    string
	StartWhiteBackground   string
	RevertBackgroundColor  string
}

// Codes that can be printed directly to apply formatting elements in terminals that support the features.
// They will not work until Inlines.Initialize is called.
// If you want to prevent inline codes from being enabled, set Inlines.Initialize to an empty function.
var Inlines = inlines{}

func initInlines() {
	// Colors need to be reset each time to account for NO_COLOR environment variable
	if noColor() {
		Inlines.StartBlackText = ""
		Inlines.StartRedText = ""
		Inlines.StartGreenText = ""
		Inlines.StartYellowText = ""
		Inlines.StartBlueText = ""
		Inlines.StartMagentaText = ""
		Inlines.StartCyanText = ""
		Inlines.StartWhiteText = ""
		Inlines.RevertTextColor = ""

		Inlines.StartBlackBackground = ""
		Inlines.StartRedBackground = ""
		Inlines.StartGreenBackground = ""
		Inlines.StartYellowBackground = ""
		Inlines.StartBlueBackground = ""
		Inlines.StartMagentaBackground = ""
		Inlines.StartCyanBackground = ""
		Inlines.StartWhiteBackground = ""
		Inlines.RevertBackgroundColor = ""
	} else {
		Inlines.StartBlackText = buildCode(black.text)
		Inlines.StartRedText = buildCode(red.text)
		Inlines.StartGreenText = buildCode(green.text)
		Inlines.StartYellowText = buildCode(yellow.text)
		Inlines.StartBlueText = buildCode(blue.text)
		Inlines.StartMagentaText = buildCode(magenta.text)
		Inlines.StartCyanText = buildCode(cyan.text)
		Inlines.StartWhiteText = buildCode(white.text)
		Inlines.RevertTextColor = buildCode(def.text)

		Inlines.StartBlackBackground = buildCode(black.bg)
		Inlines.StartRedBackground = buildCode(red.bg)
		Inlines.StartGreenBackground = buildCode(green.bg)
		Inlines.StartYellowBackground = buildCode(yellow.bg)
		Inlines.StartBlueBackground = buildCode(blue.bg)
		Inlines.StartMagentaBackground = buildCode(magenta.bg)
		Inlines.StartCyanBackground = buildCode(cyan.bg)
		Inlines.StartWhiteBackground = buildCode(white.bg)
		Inlines.RevertBackgroundColor = buildCode(def.bg)
	}

	// Styles only need to be set once
	if inlinesInitialized {
		return
	}
	Inlines.StartBold = buildCode(bold.start)
	Inlines.EndBold = buildCode(bold.end)

	Inlines.StartItalics = buildCode(italics.start)
	Inlines.EndItalics = buildCode(italics.end)

	Inlines.StartUnderline = buildCode(underline.start)
	Inlines.EndUnderline = buildCode(underline.end)

	Inlines.StartStrikethrough = buildCode(strikethrough.start)
	Inlines.EndStrikethrough = buildCode(strikethrough.end)

	Inlines.StartDim = buildCode(dim.start)
	Inlines.EndDim = buildCode(dim.end)

	Inlines.StartBlinking = buildCode(blinking.start)
	Inlines.EndBlinking = buildCode(blinking.end)

	Inlines.StartInvert = buildCode(invert.start)
	Inlines.EndInvert = buildCode(invert.end)

	Inlines.ResetFormat = buildCode("0")

	inlinesInitialized = true
}

func init() {
	Inlines.Initialize = initInlines
}
