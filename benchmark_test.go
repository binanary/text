package textfmt_test

import (
	"testing"

	"gitlab.com/binanary/textfmt"
)

var (
	s  = "This is a test string to apply formatting elements to."                   // String for tests
	tc = textfmt.Colors.Blue                                                           // Text color for tests
	bc = textfmt.Colors.Green                                                          // Background color for tests
	fs = []textfmt.Style{textfmt.Styles.Bold, textfmt.Styles.Italics, textfmt.Styles.Underline} // Styles for tests
	f  = textfmt.MakeFormat(tc, bc, fs...)                                             // Format for tests

)

func BenchmarkApplyStyles(b *testing.B) {
	b.Run("ApplyAll", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			textfmt.Apply.ColorsAndStyles(s, tc, bc, fs...)
		}
	})
	b.Run("Format", func(b *testing.B) {
		f := textfmt.MakeFormat(tc, bc, fs...)
		for n := 0; n < b.N; n++ {
			f.Sprint(s)
		}
	})
}

// These are the metrics that will be used evaluate the performance of code changes.
// Design decisions are not solely based on performance, but it is an important factor.
func BenchmarkCommonTasks(b *testing.B) {
	b.Run("Create Format", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			textfmt.MakeFormat(tc, bc, fs...)
		}
	})
	b.Run("Apply C&S", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			textfmt.Apply.ColorsAndStyles(s, tc, bc, fs...)
		}
	})
	b.Run("Format Text", func(b *testing.B) {
		for n := 0; n < b.N; n++ {
			f.Sprint(s)
		}
	})
}

func BenchmarkControl(b *testing.B) {
	for n := 0; n < b.N; n++ {
		continue
	}
}
